package com.raaabbit.realmrecyclerviewtest;

import android.support.v7.widget.RecyclerView;

/**
 *
 */
public interface ItemTouchHelperAdapter {
    boolean onItemMoved(int fromPosition, int toPosition);

    void onItemSwiped(RecyclerView.ViewHolder holder, int direction);
}
