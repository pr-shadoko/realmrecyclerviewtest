package com.raaabbit.realmrecyclerviewtest;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.Menu;
import android.view.MenuItem;

import io.realm.Realm;
import io.realm.RealmConfiguration;

public class MainActivity extends AppCompatActivity {
    ItemTouchHelper itemTouchHelper;
    RecyclerView recyclerView;
    TagsAdapter adapter;
    Realm realm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        RealmConfiguration config = new RealmConfiguration.Builder(this)
//                                    .schemaVersion(3)
//                                    .migration(new Migration())
                                    .build();
        Realm.setDefaultConfiguration(config);
        realm = Realm.getDefaultInstance();

        adapter = new TagsAdapter(realm, new OnDragStartListener() {
            @Override
            public void onDragStart(RecyclerView.ViewHolder viewHolder) {
                itemTouchHelper.startDrag(viewHolder);
            }
        });
        itemTouchHelper = new ItemTouchHelper(new TagTouchHelperCallback(adapter));
        recyclerView = (RecyclerView) findViewById(R.id.rv);
        recyclerView.setAdapter(adapter);
        itemTouchHelper.attachToRecyclerView(recyclerView);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.action_add:
                Tag tag = new Tag();
                tag.setTag("#tag" + tag.getTagId());
                realm.beginTransaction();
                realm.copyToRealmOrUpdate(tag);
                realm.commitTransaction();
                adapter.notifyItemInserted(1);
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
