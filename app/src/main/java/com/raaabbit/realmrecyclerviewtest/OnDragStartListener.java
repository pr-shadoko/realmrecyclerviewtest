package com.raaabbit.realmrecyclerviewtest;

import android.support.v7.widget.RecyclerView;

/**
 *
 */
public interface OnDragStartListener {
    void onDragStart(RecyclerView.ViewHolder viewHolder);
}
