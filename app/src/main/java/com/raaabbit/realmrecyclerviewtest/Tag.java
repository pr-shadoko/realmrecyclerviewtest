package com.raaabbit.realmrecyclerviewtest;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.RealmResults;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

/**
 *
 */
public class Tag extends RealmObject {
    @Ignore
    private final static Object nextIdLock = new Object();
    @Ignore
    private static Integer nextId;

    @PrimaryKey
    @Required
    private Integer tagId;
    @Required
    private Integer order;
    @Required
    private String tag;

    public Tag() {
        synchronized(nextIdLock) {
            if(nextId == null) {
                Realm realm = Realm.getDefaultInstance();
                RealmResults<Tag> tags = realm.where(Tag.class).findAll();
                if(tags.size() != 0) {
                    nextId = tags.max("tagId").intValue() + 1;
                } else {
                    nextId = 0;
                }
                realm.close();
            }
            order = tagId = nextId++;
        }
    }

    public Integer getTagId() {
        return tagId;
    }

    public Tag setTagId(Integer tagId) {
        this.tagId = tagId;
        return this;
    }

    public Integer getOrder() {
        return order;
    }

    public Tag setOrder(Integer order) {
        this.order = order;
        return this;
    }

    public String getTag() {
        return tag;
    }

    public Tag setTag(String tag) {
        this.tag = tag;
        return this;
    }
}