package com.raaabbit.realmrecyclerviewtest;

import android.support.v4.view.MotionEventCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;

/**
 *
 */
public class TagsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements ItemTouchHelperAdapter {
    private static final int TYPE_TAG = 0;
    private static final int TYPE_HEADER = 1;

    private Realm realm;
    private RealmResults<Tag> tags;
    private OnDragStartListener dragStartListener;

    public TagsAdapter(Realm realm, OnDragStartListener dragStartListener) {
        this.realm = realm;
        this.tags = realm.where(Tag.class).findAllSorted("order", Sort.DESCENDING);
        this.dragStartListener = dragStartListener;

        setHasStableIds(true);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch(viewType) {
            case TYPE_TAG:
                View taskView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item, parent, false);
                return new TagViewHolder(taskView);
            case TYPE_HEADER:
                View headerView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item, parent, false);
                return new HeaderViewHolder(headerView);
            default:
                return null;
        }
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int adapterPosition) {
        switch(holder.getItemViewType()) {
            case TYPE_TAG:
                onBindViewHolderTag((TagViewHolder) holder, adapterPosition);
                break;
            case TYPE_HEADER:
                onBindViewHolderHeader((HeaderViewHolder) holder, adapterPosition);
                break;
            default:
        }
    }

    private void onBindViewHolderTag(final TagViewHolder holder, final int adapterPosition) {
        final Tag tag = tags.get(getDataSetPosition(adapterPosition));
        holder.title.setText(tag.getTag());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("TAGS", "item clicked: id=" + tag.getTagId() + " ; adapterPosition=" + adapterPosition + " ; datasetPosition=" + getDataSetPosition(adapterPosition));
            }
        });
        holder.itemView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(MotionEventCompat.getActionMasked(event) == MotionEvent.ACTION_DOWN) {
                    dragStartListener.onDragStart(holder);
                }
                return false;
            }
        });
    }

    private void onBindViewHolderHeader(HeaderViewHolder holder, int adapterPosition) {
        holder.header.setText("TITLE");
    }

    private int getDataSetPosition(int adapterPosition) {
        switch(getItemViewType(adapterPosition)) {
            case TYPE_TAG:
                return adapterPosition - 1;
            case TYPE_HEADER:
            default:
                return RecyclerView.NO_POSITION;
        }
    }

    @Override
    public int getItemCount() {
        return tags.size() + 1;
    }

    @Override
    public long getItemId(int adapterPosition) {
        switch(getItemViewType(adapterPosition)) {
            case TYPE_TAG:
                return tags.get(getDataSetPosition(adapterPosition)).getTagId();
            case TYPE_HEADER:
            default:
                return RecyclerView.NO_ID;
        }
    }

    @Override
    public int getItemViewType(int adapterPosition) {
        if(adapterPosition == 0) {
            return TYPE_HEADER;
        }
        return TYPE_TAG;
    }

    @Override
    public boolean onItemMoved(final int fromAdapterPosition, final int toAdapterPosition) {
        Log.i("TAGS", "move from " + fromAdapterPosition + " to " + toAdapterPosition);
        if(getItemViewType(toAdapterPosition) != TYPE_TAG) {
            return false;
        }

        Tag task1 = tags.get(getDataSetPosition(fromAdapterPosition));
        Tag task2 = tags.get(getDataSetPosition(toAdapterPosition));
        Log.i("TAGS", "realm pos from=" + getDataSetPosition(fromAdapterPosition) + " ; to=" + getDataSetPosition(toAdapterPosition));
        int order1 = task1.getOrder();
        realm.beginTransaction();
        task1.setOrder(task2.getOrder());
        task2.setOrder(order1);
        realm.copyToRealmOrUpdate(task1);
        realm.copyToRealmOrUpdate(task2);
        realm.commitTransaction();
        notifyItemMoved(fromAdapterPosition, toAdapterPosition);
        return true;
    }

    @Override
    public void onItemSwiped(RecyclerView.ViewHolder holder, int direction) {}

    public class TagViewHolder extends RecyclerView.ViewHolder {
        public final TextView title;

        public TagViewHolder(View itemView) {
            super(itemView);
            this.title = (TextView) itemView.findViewById(R.id.label);
        }
    }

    public class HeaderViewHolder extends RecyclerView.ViewHolder {
        public final TextView header;

        public HeaderViewHolder(View itemView) {
            super(itemView);
            this.header = (TextView) itemView.findViewById(R.id.label);
        }
    }
}